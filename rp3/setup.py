try:
    from setuptools import setup
except ImportError:
    from disutils.core import setup

config = {
        'description': 'RP3 data analysis',
        'author': 'Janis Gailis',
        'url': 'http://142.93.229.196',
        'download_url': 'http://142.93.229.196/download/rp3',
        'author_email': 'janis.gailis@uia.no',
        'version': '0.1',
        'install_requires': ['nose'],
        'packages': ['rp3'],
        'scripts': [],
        'name': 'rp3anlysis'
}

setup(**config)
