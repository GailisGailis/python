import csv
import datetime
import numpy as np
import matplotlib.pyplot as plt
//from scipy.interpolate import BSpline

athlete_initials = "Roer"
rp3_csv_file = "syvert2019111540minLK.csv"

def checkNE(value):
    return value == '';

delimiter = ','
with open(rp3_csv_file,'r') as dest_f:
    data_iter = csv.reader(dest_f, 
                           delimiter = delimiter, 
                           quotechar = '"')
    data = [data for data in data_iter]
    
#stroke_number_array    = []
#curve_data_array      = []

for e in data:
    #stroke_number = e[3]
    curve_data = e[22]
    curve_data_points_list = [int(x) for x in curve_data.split(",") if x.strip().isdigit()] #elegant, kan nevnes som eksempel i IS-105
    y = np.array(curve_data_points_list)
    x = np.arange(len(y))
    plt.plot(x, y, linewidth=1)
 #   stroke_number_array.append(stroke_number)
 #   curve_data_array.append(curve_data)

# Put the title and labels
plt.title('Power Curve', color='black')
plt.xlabel('Data point', color='blue')
plt.ylabel('Power (N)', color='blue')
#plt.legend(loc=2)
#plt.grid()

plt.show()


def test():
    print(stroke_number_array)
    print(curve_data_array)

    print(len(stroke_number_array))
    print(len(curve_data_array))
